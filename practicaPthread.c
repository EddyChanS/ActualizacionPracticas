#include<pthread.h>//biblioteca pthread a implementar
#include<stdio.h>
#include<time.h>//para la fecha actual
#include <stdlib.h>
#include <unistd.h>//biblioteca para imprimir el hostname

#define CANT_THREADS 2//puede variar la cantidad de threads

//Aqui se define lo que realizara el hilo explicito
void *funcionDeTrabajo(void *n)//atributo el nùmero de hilos
{
	long id = (long) n;//El identificador de este hilo
	printf("Soy el thread %ld!\n", id);

	//Bloque para sacar la fecha actual desde c
	time_t tiempo = time(0);
   struct tm *tlocal = localtime(&tiempo);
   char output[128];
   strftime(output,128,"%d/%m/%y %H:%M:%S",tlocal);
   printf("%s\n",output);

	//Bloque para el hostname de la maquina
	char hostname[1024];
   gethostname(hostname, 1024);
	puts(hostname);
	printf("Integrantes del equipo:\nEdson Servin Mote\nEdgar Tapia Peralta\nMarco Antonio Estrada Robles\n\n");

	pthread_exit(NULL);
}

int main(int argc, char **argv)
{
	//Se declara un arreglo de threads con sus id's
	pthread_t thread[CANT_THREADS]; 
	long tid;
	int rc;

	//se crean los threads correspondientes
	for (tid = 0; tid < CANT_THREADS; ++tid)
		rc = pthread_create(&thread[tid], NULL, funcionDeTrabajo, (void *)tid);

	//Se arrancan
	for (tid = 0; tid < CANT_THREADS; ++tid)
		pthread_join(thread[tid], NULL);

	pthread_exit(NULL);
}
