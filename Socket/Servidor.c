#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>

int main(int argc, char **argv)
{

struct sockaddr_in server;
struct sockaddr_in client;
int fd,fd2,longitud_cliente,numbytes,puerto=5000;
char buf[1024]; //Para recibir mensaje
char enviar2[1024]; //Para enviar mensaje
char enviar1[1024]; //Para enviar mensaje
 
system("clear");
 
 server.sin_family= AF_INET;
 server.sin_port = htons(puerto);
 server.sin_addr.s_addr = INADDR_ANY;
 bzero(&(server.sin_zero),8);
 
 //Socket
 if (( fd=socket(AF_INET,SOCK_STREAM,0) )<0){
 perror("Error de apertura de socket");
 exit(-1);
 }
 
 //Se creo Socket
 if(bind(fd,(struct sockaddr*)&server, sizeof(struct sockaddr))==-1) {
 printf("error en bind() \n");
 exit(-1);
 }
 
 //Socket en modo escucha
 if(listen(fd,5) == -1) {
 printf("error en listen()\n");
 exit(-1);
 }
 
 printf("servidor en espera...\n");
 longitud_cliente= sizeof(struct sockaddr_in);
 if ((fd2 = accept(fd,(struct sockaddr *)&client,&longitud_cliente))==-1) {
 printf("error en accept()\n");
 exit(-1);
 }
 
 printf("------SESION INICIADA------\n");
 printf("cliente conectado\n");
 strcpy(enviar1,"servidor conectado...");
 send(fd2, enviar1, 1024,0);
 
//Ciclo para enviar y recibir mensajes
 while(1){
 //El servidor espera el primer mensaje
 recv(fd2,buf,1024,0);
 if(strcmp(&buf,"salir")==0){
 break;
 }
 printf("Cliente: %s\n",buf);
 
 //El cliente recibe el mensaje del servidor
 printf("Escribir mensaje: ");
 scanf("%*c%[^\n]",enviar2);
 send(fd2,enviar2,1024,0);
 if(strcmp(enviar2,"salir")==0){
 break;
 }
}
 close(fd2);
 close(fd);
return 0;
}
