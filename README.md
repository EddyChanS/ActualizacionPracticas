Edson Servin Mote
308292503

Preparación del sistema
se verificó que el contenido del archivo source.list contenga los repositorios y versión correcta, con aptitude update se actualizó la información de los repositorios de paquetes.

Instalación de dependencias
Se instalaron las dependecias para compilar el kernerl sin problemas,  posteriormente se descargó el tarball de kernell, firma GPF, se procedió a confirmar la firma de tarball con gpg, se desempaquetó tarball y se verificaron sus archivos extraidos sin problemas y se configuró el kernel, por ultimo se compilo el kernel con make all.

Creación del paquete deb
primero se ejecutó make deb-pkg para la generación de los paquetes de instalación del kernel y no genero error

Instalación de kernel
Se instalaron los paquetes con dpkg -i
se listaron los paquetes con aptitude y se verificó el contenido del directorio sin problemas

hasta esta parte se procedió normal, sin embargo se pausó la maquina virtual y al volver a reiniciarla y querer listar de nuevo y proceder con la instalación ya no aparecieron los paquetes por lo que no se pudo seguir con la instalación.

se anexan imagenes del proceso

nota: se clono la maquina virtual para mi compañero Edgar Tapia Peralta y aplicaba el mismo error, se volvió a la instalación de dependencias y creación del paquete pero aún y así no se encontraban los paquetes.
